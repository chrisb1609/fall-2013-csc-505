package csc505.project.floyd;

/**
 * Created by nazareth on 11/23/13.
 */
public class matrices {
    private float[][] weights;
    private float[][] parent;

    public matrices(float[][] weights, float[][] parent) {
        this.weights = weights;
        this.parent = parent;

    }

    public float[][] getParent() {
        return parent;
    }

    public float[][] getWeights() {
        return weights;
    }
}

