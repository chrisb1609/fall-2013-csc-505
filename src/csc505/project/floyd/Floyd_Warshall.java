package csc505.project.floyd;
        /* http://docs.oracle.com/javase/tutorial/essential/io/scanning.html
*/
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class Floyd_Warshall {


    /* Referred to the Java tutorial for how to read a file using Scanner
     The link for it -
     http://docs.oracle.com/javase/tutorial/essential/io/scanning.html */

    public static matrices readFileWarshall(String filename) {
        Scanner scanner = null;

        int numVerts = 0;

        float[][] matrix;
        matrix = new float[0][];
        float[][] parent_matrix;
        parent_matrix = new float[0][];
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(filename)));

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                if (line.startsWith("c") && numVerts == 0) {
                    String[] pieces = line.split(" ");
                    numVerts = Integer.parseInt(pieces[2]);
                    matrix = new float[numVerts + 1][numVerts + 1];
                    matrix = initializeMatrix(matrix, 0);
                    parent_matrix = new float[numVerts + 1][numVerts + 1];
                    parent_matrix = initializeMatrix(parent_matrix, 1);
                    continue;
                }


                if (line.startsWith("e")) {
                    String[] pieces = line.split(" ");
                    float weight = Float.valueOf(pieces[3]);
                    int startId = Integer.parseInt(pieces[1]);
                    int endId = Integer.parseInt(pieces[2]);
                    matrix[startId][endId] = weight;
                    parent_matrix[startId][endId] = startId;
                }

            }


        } catch (Exception e) {
            System.err.println("Exception: " + e.getClass().toString() + " - " + e.getMessage());
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return new matrices(matrix, parent_matrix);
    }

    private static float[][] initializeMatrix(float[][] new_matrix, int flag) {
        int n = new_matrix.length;
        //flag == 0 is for initializing distance matrix
        if (flag == 0) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j) {
                        new_matrix[i][j] = 0;
                    } else new_matrix[i][j] = Float.POSITIVE_INFINITY;
                }
            }
        }
        //flag == 1 is to initialize parent matrix
        else if (flag == 1) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    new_matrix[i][j] = 0;
                }
            }
        }
        return new_matrix;
    }

    public static matrices floydWarshall(float[][] floyd_matrix, float[][] parent_matrix) {
        int n = floyd_matrix.length;
        for (int k = 1; k < n; k++) {
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    if (floyd_matrix[i][k] + floyd_matrix[k][j] < floyd_matrix[i][j]) {
                        floyd_matrix[i][j] = floyd_matrix[i][k] + floyd_matrix[k][j];
                        parent_matrix[i][j] = k;
                    }
                }
            }

        }

        return new matrices(floyd_matrix, parent_matrix);
    }

    static void printMatrix(float[][] matrix) {
        int n = matrix.length;
        System.out.println("Length of the Matrix - " + n);
        for (int i = 1; i < n; i++) {
            System.out.print("[");
            for (int j = 1; j < n; j++) {
                System.out.print("["+ matrix[i][j] + "]");
            }
            System.out.println("]");
        }
        System.out.println("");
    }

    public static matrices main(String fileName) {

        matrices floyd_matrix = readFileWarshall(fileName);
        long start = System.currentTimeMillis();
        matrices container = floydWarshall(floyd_matrix.getWeights(), floyd_matrix.getParent());
        long end = System.currentTimeMillis();
        System.out.println("Run  Time for Floyd Warshall: " + (end - start) + " ms");
        /*System.out.println("Floyd Warshall Matrix - ");
        printMatrix(container.getWeights());
        System.out.println("Parent Matrix - ");
        printMatrix(container.getParent());*/
        return container;
    }

}
