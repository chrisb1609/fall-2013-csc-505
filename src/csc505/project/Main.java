package csc505.project;
import csc505.project.floyd.*;
import csc505.project.dijkstra.*;
/**
 * Created by nazareth on 11/23/13.
 */
public class Main {

    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("Please Provide the Filename -  ");
                System.exit(0);
            }
            matrices floyd = Floyd_Warshall.main(args[0]);
            ShortestPaths dijkstras = Dijkstras.main(args[0]);
            System.out.println("Size of Matrix for Floyd - " + floyd.getParent().length);
            System.out.println("Size of Matrix for Dijkstras - " + dijkstras.getParents().length);

            //if(!arraysEqual(floyd.getWeights(), dijkstras.getWeights()))
              //  System.out.println("Weight Arrays NOT Equal.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean arraysEqual(float[][] a1, float[][] a2) {
        if (a1.length != a2.length || a1[0].length != a2[0].length)
            return false;

        for (int i = 1; i < a1.length; i++) {
            for (int j = 1; j < a1.length; j++) {
                if (a1[i][j] != a2[i][j])
                    return false;
            }
        }

        return true;
    }
}
