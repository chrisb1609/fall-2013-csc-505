package csc505.project.dijkstra;

public class Edge {
	private float weight;
	private Vertex start;
	private Vertex end;
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public Vertex getStart() {
		return start;
	}
	public void setStart(Vertex start) {
		this.start = start;
		start.getAdjacentEdges().add(this);
	}
	public Vertex getEnd() {
		return end;
	}
	public void setEnd(Vertex end) {
		this.end = end;
	}
}
