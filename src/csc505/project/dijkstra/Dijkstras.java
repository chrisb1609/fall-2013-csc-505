package csc505.project.dijkstra;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;


public class Dijkstras {

    /* Referred to the Java tutorial for how to read a file using Scanner
     The link for it -
     http://docs.oracle.com/javase/tutorial/essential/io/scanning.html */

    public static Graph readFile(String filename) {
		Graph G = new Graph();
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(filename)));

			long numVerts = -1;
			
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				if (line.startsWith("c") && numVerts == -1) {
					String[] pieces = line.split(" ");
					numVerts = Long.parseLong(pieces[2]);
					
					for (int id = 1; id <= numVerts; id++) {
						Vertex v = new Vertex();
						v.setId(id);
						v.setDistance(Float.POSITIVE_INFINITY);
						G.getVertices().add(v);
					}
					
					continue;
				} else if (line.startsWith("g")) {
					continue;
				}
				
				String[] pieces = line.split(" ");
			
				float weight = Float.valueOf(pieces[3]);
				int startId = Integer.parseInt(pieces[1]);
				int endId = Integer.parseInt(pieces[2]);
				
				Edge e = new Edge();
				e.setWeight(weight);
				e.setStart(G.getVertices().get(startId));
				e.setEnd(G.getVertices().get(endId));
				G.getEdges().add(e);
			}
			
		
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			if (scanner != null)
				scanner.close();
		}
		
		return G;
	}

		public static ShortestPaths main(String fileName) {
		Graph g = readFile(fileName);

		long start = System.currentTimeMillis();
		ShortestPaths sp = doAllPairsDijkstras(g);
		long finish = System.currentTimeMillis();
		
		System.out.println("Run time for Dijkstra: " + (finish - start) + "ms");
		
	    //printAllPairsResults(sp);
        return sp;
	}

	public static void initialize(Graph G) {
		for (int i = 1; i < G.getVertices().size(); i++) {
            Vertex v = G.getVertices().get(i);
			v.setDistance(Float.POSITIVE_INFINITY);
			v.setParent(null);
		}
	}
	
	public static ShortestPaths doAllPairsDijkstras(Graph G) {
		ShortestPaths sp = new ShortestPaths(G);

        for (int i = 1; i < G.getVertices().size(); i++) {
            doDijkstras(G, G.getVertices().get(i));

            for (int j = 1; j < G.getVertices().size(); j++) {
                sp.getLengths()[i][j] = G.getVertices().get(j).getDistance();
                Vertex p = G.getVertices().get(j).getParent();

                if (p != null)
                    sp.getParents()[i][j] = p.getId();
            }
        }


		return sp;
	}
	
	public static void doDijkstras(Graph G, Vertex start) {
		//Initialize a queue of vertices
		//Set start vertex == 0 distance
		//relax edges until done
		
		initialize(G);
		PriorityQueue<Vertex> Q = new PriorityQueue<Vertex>();
		start.setDistance(0);

        for (int i = 1; i < G.getVertices().size(); i++)
            Q.add(G.getVertices().get(i));


		while (!Q.isEmpty()) {
			Vertex v = Q.remove();
			
			for (Edge e : v.getAdjacentEdges()) {
				relax(e, Q);
			}
		}
	}
	
	private static void relax(Edge e, PriorityQueue<Vertex> Q) {
		if (e.getEnd().getDistance() > e.getStart().getDistance() + e.getWeight()) {
			e.getEnd().setDistance(e.getStart().getDistance() + e.getWeight());
			e.getEnd().setParent(e.getStart());
			
			//Update the queue
			Q.remove(e.getEnd());
			Q.add(e.getEnd());
		}
			
	}
	
	public static void printResults(Graph G) {
		List<Vertex> vertices = G.getVertices();
		System.out.println("====Shortest Distances====");
		
		for (int i = 1; i < G.getVertices().size(); i++) {
            Vertex v = G.getVertices().get(i);
			System.out.println("[" + v.getId() + "] - " + v.getDistance());
		}
	}

    public static void printAllPairsResults(ShortestPaths sp) {
        for (int i = 1; i < sp.getLengths().length; i++) {
            System.out.print("[");
            for (int j = 1; j < sp.getLengths().length; j++) {
                System.out.print("[" + sp.getLengths()[i][j] + "]");
            }
            System.out.println("]");
        }

        System.out.println();

        for (int i = 1; i < sp.getParents().length; i++) {
            System.out.print("[");
            for (int j = 1; j < sp.getParents()[0].length; j++) {
                System.out.print("[" + sp.getParents()[i][j] + "]");
            }
            System.out.println("]");
        }
    }
}
