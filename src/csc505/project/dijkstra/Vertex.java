package csc505.project.dijkstra;

import java.util.ArrayList;
import java.util.List;


public class Vertex implements Comparable<Vertex> {
	private float distance;
	private Vertex parent;
	private float id;
	private List<Edge> adjacentEdges;
	
	public Vertex() {
		this.adjacentEdges = new ArrayList<Edge>();
	}

	public List<Edge> getAdjacentEdges() {
		return adjacentEdges;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public Vertex getParent() {
		return parent;
	}

	public void setParent(Vertex parent) {
		this.parent = parent;
	}

	public float getId() {
		return id;
	}

	public void setId(float id) {
		this.id = id;
	}

	public void setAdjacentEdges(List<Edge> adjacentEdges) {
		this.adjacentEdges = adjacentEdges;
	}

	@Override
	public int compareTo(Vertex arg0) {
		if (this.getDistance() < arg0.getDistance())
			return -1;
		if (this.getDistance() > arg0.getDistance())
			return 1;
		
		return 0;
	}
	
}
