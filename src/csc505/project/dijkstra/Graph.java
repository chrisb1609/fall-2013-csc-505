package csc505.project.dijkstra;

import java.util.ArrayList;
import java.util.List;


public class Graph {
	private List<Vertex> vertices;
	private List<Edge> edges;
	
	public Graph() {
		vertices = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();

        //This will make our vertex position match the id
        // and give us 1 as the start index
        vertices.add(null);
        edges.add(null);
	}

	public List<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(List<Vertex> vertices) {
		this.vertices = vertices;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
}
