package csc505.project.dijkstra;

/**
 * Created with IntelliJ IDEA.
 * User: chris
 * Date: 11/19/13
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortestPaths {
    private float[][] lengths;
    private float[][] parents;

    public ShortestPaths(Graph G) {
        this.lengths = new float[G.getVertices().size()][G.getVertices().size()];
        this.parents = new float[G.getVertices().size()][G.getVertices().size()];
    }

    public float[][] getLengths() {
        return lengths;
    }

    public void setLengths(float[][] lengths) {
        this.lengths = lengths;
    }

    public float[][] getParents() {
        return parents;
    }

    public float[][] getWeights(){ return lengths;}

    public void setParents(float[][] parents) {
        this.parents = parents;
    }



}
