package csc505.project;

import csc505.project.dijkstra.*;
import csc505.project.floyd.*;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: chris
 * Date: 11/23/13
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 *
 * Thank you Mkyong.com for how to run command line commands from Java
 *          http://www.mkyong.com/java/how-to-execute-shell-command-from-java/
 *
 */
public class Experiment {
    public static void main(String [] args) throws Exception {
        if (args.length != 1) {
            System.out.println("USAGE: java Experiment <full path to nextgen executable>");
            return;
        }

        Random random = new Random();

        String nextGen = args[0];
        String testFileName = "csc505testGraph.txt";

        System.out.println("Algorithm, Density, Number of Vertices, Number of Edges, Random Seed, Running Time (ms)");


        //For every number of vertex level, do 5 sparse runs and then 5 dense runs on both algorithms
        for (int numVerts = 500; numVerts <= 2500; numVerts+=500) {

            long numSparseEdges = numVerts * 2;
            long floydTime = 0;
            long dijkstraTime = 0;
            for (int run = 1; run <= 5; run++) {
                int randomSeed = random.nextInt();
                generateTestFile(nextGen, testFileName, numVerts, numSparseEdges, randomSeed);

                //Get time for Floyd
                long runfloydTime = runFloydWarshall(testFileName);
                floydTime += runfloydTime;
                System.out.println("Floyd-Warshall, Sparse, " + numVerts + ", " + numSparseEdges + ", " + randomSeed + ", " + runfloydTime);

                //Get time for Dijkstras
                long rundijkstraTime = runDijkstras(testFileName);
                dijkstraTime += rundijkstraTime;
                System.out.println("Dijkstras, Sparse, " + numVerts + ", " + numSparseEdges + ", " + randomSeed + ", " + rundijkstraTime);
            }
            System.out.println("Average Time for Floyd in the current setting for Sparse Graphs- " + floydTime/5);
            System.out.println("Average Time for Dijkstra in the current setting for Sparse Graphs- " + dijkstraTime/5);


            long numDenseEdges = Math.round(Math.pow((double)numVerts, 1.8));
            dijkstraTime = 0;
            floydTime = 0;
            for (int run = 1; run <= 5; run++) {
                int randomSeed = random.nextInt();
                generateTestFile(nextGen, testFileName, numVerts, numDenseEdges, randomSeed);

                //Get time for Floyd
                long runfloydTime = runFloydWarshall(testFileName);
                floydTime += runfloydTime;
                System.out.println("Floyd-Warshall, Dense, " + numVerts + ", " + numDenseEdges + ", " + randomSeed + ", " + runfloydTime);

                //Get time for Dijkstras
                long rundijkstraTime = runDijkstras(testFileName);
                dijkstraTime += rundijkstraTime;
                System.out.println("Dijkstras, Dense, " + numVerts + ", " + numDenseEdges  + ", " + randomSeed + ", " + rundijkstraTime);
            }
            System.out.println("Average Time for Floyd in the current setting for Dense Graphs - " + floydTime/5);
            System.out.println("Average Time for Dijkstra in the current setting for Dense Graphs - " + dijkstraTime/5);

        }
    }

    public static long runFloydWarshall(String filename) {
        matrices floyd_matrix = Floyd_Warshall.readFileWarshall(filename);
        long start = System.currentTimeMillis();
        matrices container = Floyd_Warshall.floydWarshall(floyd_matrix.getWeights(), floyd_matrix.getParent());
        long end = System.currentTimeMillis();
        return end - start;
    }

    public static long runDijkstras(String filename) {
        Graph G = Dijkstras.readFile(filename);

        long startTime = System.currentTimeMillis();
        ShortestPaths sp = Dijkstras.doAllPairsDijkstras(G);
        long endTime = System.currentTimeMillis();

        return endTime - startTime;
    }

    public static void generateTestFile(String testCommand, String filename, long numVerts, long numEdges, long randomSeed) throws Exception {
        Process testFileProc = Runtime.getRuntime().exec(testCommand + " random " + numVerts + " " + numEdges + " " + randomSeed + " " + filename);
        testFileProc.waitFor();
    }

}
